# config

A library to provide a configuration objects by key values, it also supports different data types.

## Requirements

* PHP 7.1

## Simple usage

```php
<?php

$config = new \MittNett\Config\Config('somekey', new \MittNett\Config\Type\StringType());
$config->setValue('lol');
```

## ORM Integrations

Optionally, you can also install doctrine/orm to use the provided annotation mappings in this library.

## License

MIT License, see [LICENSE.md]
