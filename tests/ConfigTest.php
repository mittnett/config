<?php

declare(strict_types=1);

namespace MittNett\Config\Tests;

use MittNett\Config\Config;

/**
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class ConfigTest extends TestCase
{
    public function testFields()
    {
        $config = new Config('somekey');
        $this->assertSame('somekey', $config->getKey());
    }

    public function testInheritFields()
    {
        $parentConfig = new Config('parent');
        $config = new Config('somekey', $parentConfig);
        $this->assertSame('parent', $config->getInherits()->getKey());
    }

    public function testSetValue()
    {
        $config = new Config('somekey');

        $valuesToTest = [
            'a value', 1, 2, '01', 1.0, .865, 'øæå', "\n", "\r\n"
        ];

        foreach ($valuesToTest as $value) {
            $config->setValue($value);
            $this->assertSame($value, $config->getValue());
        }
    }

    public function testSetValueInherit()
    {
        $parentConfig = new Config('parent');
        $config = new Config('somekey', $parentConfig);

        $valuesToTest = [
            'a value', 1, 2, '01', 1.0, .865, 'øæå', "\n", "\r\n"
        ];

        foreach ($valuesToTest as $value) {
            $config->setValue($value);
            $this->assertSame($value, $config->getValue());
            $this->assertSame($value, $parentConfig->getValue());
        }
    }
}
