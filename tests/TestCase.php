<?php

declare(strict_types=1);

namespace MittNett\Config\Tests;

use MittNett\Config\Type\Registry;
use PHPUnit\Framework\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    protected function getRegistry(): Registry
    {
        $registry = new Registry();
        $registry->addDefaults();

        return $registry;
    }
}
