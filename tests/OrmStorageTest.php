<?php

declare(strict_types=1);

namespace MittNett\Config\Tests;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use MittNett\Config\Config;
use MittNett\Config\OrmStorage;
use MittNett\Config\ArrayStorage;
use MittNett\Config\NotFoundException;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Mockery as m;

/**
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class OrmStorageTest extends MockeryTestCase
{
    public function tearDown()
    {
        m::close();
    }

    public function testStoring()
    {
        $config = m::mock(Config::class);
        $config->shouldReceive('getKey')->andReturn('somekey');

        $repo = m::mock(EntityRepository::class);
        $repo->shouldReceive('find')->with('somekey')->andReturn($config);
        $repo->shouldReceive('findAll')->andReturn([ $config ]);

        $em = m::mock(EntityManagerInterface::class);
        $em->shouldReceive('persist')->with($config)->once();
        $em->shouldReceive('getRepository')->with(Config::class)->andReturn($repo);

        $storage = new OrmStorage($em, Config::class);

        $this->assertTrue($storage->addConfig($config));
        $this->assertNotEmpty($storage->getConfigs());
        $this->assertNotNull($storage->getConfig('somekey'));
        $this->assertNotNull($storage->getRequiredConfig('somekey'));
        $this->assertCount(1, $storage->getConfigs());
    }

    public function testStoringMultiple()
    {
        $config1 = m::mock(Config::class);
        $config1->shouldReceive('getKey')->andReturn('somekey1');
        $config2 = m::mock(Config::class);
        $config2->shouldReceive('getKey')->andReturn('somekey2');

        $repo = m::mock(EntityRepository::class);
        $repo->shouldReceive('find')->with('somekey1')->andReturn($config1);
        $repo->shouldReceive('find')->with('somekey2')->andReturn($config2);
        $repo->shouldReceive('findAll')->andReturn([ $config1, $config2 ]);

        $em = m::mock(EntityManagerInterface::class);
        $em->shouldReceive('persist')->with($config1)->once();
        $em->shouldReceive('persist')->with($config2)->once();
        $em->shouldReceive('getRepository')->with(Config::class)->andReturn($repo);

        $storage = new OrmStorage($em, Config::class);

        $storage->addConfigs([$config1, $config2]);
        $this->assertNotEmpty($storage->getConfigs());
        $this->assertNotNull($storage->getConfig('somekey1'));
        $this->assertNotNull($storage->getConfig('somekey2'));
        $this->assertNotNull($storage->getRequiredConfig('somekey1'));
        $this->assertNotNull($storage->getRequiredConfig('somekey2'));
        $this->assertCount(2, $storage->getConfigs());
    }


    public function testRequiredGet()
    {
        $this->expectException(NotFoundException::class);

        $repo = m::mock(EntityRepository::class);
        $repo->shouldReceive('find')->with('somekey')->andReturn(null);

        $em = m::mock(EntityManagerInterface::class);
        $em->shouldReceive('getRepository')->with(Config::class)->andReturn($repo);

        $storage = new OrmStorage($em, Config::class);
        $storage->getRequiredConfig('somekey');
    }
}
