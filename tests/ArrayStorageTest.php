<?php

declare(strict_types=1);

namespace MittNett\Config\Tests;

use MittNett\Config\Config;
use MittNett\Config\ArrayStorage;
use MittNett\Config\NotFoundException;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Mockery as m;

/**
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class ArrayStorageTest extends MockeryTestCase
{
    public function tearDown()
    {
        m::close();
    }

    public function testStoring()
    {
        $storage = new ArrayStorage();

        $config = m::mock(Config::class);
        $config->shouldReceive('getKey')->andReturn('somekey')->once();

        $this->assertTrue($storage->addConfig($config));
        $this->assertNotEmpty($storage->getConfigs());
        $this->assertNotNull($storage->getConfig('somekey'));
        $this->assertNotNull($storage->getRequiredConfig('somekey'));
        $this->assertCount(1, $storage->getConfigs());
    }

    public function testRequiredGet()
    {
        $this->expectException(NotFoundException::class);

        $storage = new ArrayStorage();
        $storage->getRequiredConfig('somekey');
    }

    public function testAddConfigs()
    {
        $storage = new ArrayStorage();

        $config1 = m::mock(Config::class)->shouldIgnoreMissing();
        $config2 = m::mock(Config::class)->shouldIgnoreMissing();
        $config3 = m::mock(Config::class)->shouldIgnoreMissing();

        $storage->addConfig($config1);
        $this->assertCount(1, $storage->getConfigs());

        $storage->addConfigs([$config2, $config3]);
        $this->assertCount(3, $storage->getConfigs());
    }
}
