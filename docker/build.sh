#!/usr/bin/env bash

(cd php && docker build -t registry.gitlab.com/mittnett/config .)
(cd php && docker push registry.gitlab.com/mittnett/config)
