<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('vendor')
    ->in(__DIR__);

return PhpCsFixer\Config::create()
    ->setRules([
        '@PSR2' => true,
        'strict_param' => true,
        'array_syntax' => ['syntax' => 'short'],
        'void_return' => false,
        'declare_strict_types' => true,
        'phpdoc_align' => true,
        'linebreak_after_opening_tag' => true,
        'full_opening_tag' => true,
    ])
    ->setRiskyAllowed(true)
    ->setFinder($finder);
