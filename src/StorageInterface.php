<?php

declare(strict_types=1);

namespace MittNett\Config;

/**
 * Represents a storage that can store and retrive configs.
 *
 * @author Edvin Hultberg <hultberg@mittnett.net>
 */
interface StorageInterface
{
    /**
     * Store a config.
     *
     * @param Config $config
     *
     * @return bool
     */
    public function addConfig(Config $config): bool;

    /**
     * Fetch a config from the storage.
     *
     * @param string $key
     *
     * @return Config|null
     */
    public function getConfig(string $key): ?Config;

    /**
     * Provides all configurations.
     *
     * @return array
     */
    public function getConfigs(): array;

    /**
     * Add multiple configs, this will not clear any previous set configs.
     *
     * @param Config[] $configs
     */
    public function addConfigs(array $configs): void;

    /**
     * Fetch a config that is considered required from the storage.
     *
     * When the config is not set, an exception is thrown.
     *
     * @throws NotFoundException
     *
     * @param string $key
     *
     * @return Config
     */
    public function getRequiredConfig(string $key): Config;
}
