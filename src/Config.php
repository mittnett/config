<?php

declare(strict_types=1);

namespace MittNett\Config;

use Doctrine\ORM\Mapping as ORM;

/**
 * The Config class that holds the key and value.
 *
 * @ORM\Entity()
 * @author Edvin Hultberg <hultberg@mittnett.net>
 */
class Config
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     * @var string
     */
    protected $key;

    /**
     * @ORM\Column(type="string", unique=true)
     * @var mixed
     */
    protected $value;

    /**
     * @ORM\ManyToOne(targetEntity="MittNett\Config\Config", inversedBy="children")
     * @var Config|null
     */
    protected $inherits;

    /**
     * @param string      $key
     * @param Config|null $inherits
     */
    public function __construct(string $key, Config $inherits = null)
    {
        $this->setKey($key);
        $this->setInherits($inherits);
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key)
    {
        $this->key = $key;
    }

    /**
     * Provides the value on this config. The value must be decoded by the type.
     *
     * @return mixed
     */
    public function getValue()
    {
        if ($this->inherits !== null) {
            return $this->inherits->getValue();
        }

        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        if ($this->inherits !== null) {
            $this->inherits->setValue($value);
        }

        $this->value = $value;
    }

    /**
     * @return Config|null
     */
    public function getInherits(): ?Config
    {
        return $this->inherits;
    }

    /**
     * @param Config|null $inherits
     */
    public function setInherits(?Config $inherits)
    {
        $this->inherits = $inherits;
    }
}
