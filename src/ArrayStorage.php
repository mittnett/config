<?php

declare(strict_types=1);

namespace MittNett\Config;

class ArrayStorage implements StorageInterface
{
    /**
     * @var Config[]
     */
    private $configs;

    public function __construct()
    {
        $this->configs = [];
    }

    /**
     * @inheritDoc
     */
    public function addConfig(Config $config): bool
    {
        $this->configs[$config->getKey()] = $config;

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getConfig(string $key): ?Config
    {
        return $this->configs[$key] ?? null;
    }

    /**
     * @inheritDoc
     */
    public function getConfigs(): array
    {
        return $this->configs;
    }

    /**
     * @inheritDoc
     */
    public function addConfigs(array $configs): void
    {
        array_push($this->configs, ...$configs);
    }

    /**
     * @inheritDoc
     */
    public function getRequiredConfig(string $key): Config
    {
        if (isset($this->configs[$key])) {
            return $this->configs[$key];
        }

        throw new NotFoundException("Config $key was not found");
    }
}
