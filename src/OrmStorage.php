<?php

declare(strict_types=1);

namespace MittNett\Config;

use Doctrine\ORM\EntityManagerInterface;

class OrmStorage implements StorageInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var string|null
     */
    private $className;

    /**
     * @param EntityManagerInterface $entityManager
     * @param string|null            $className
     */
    public function __construct(EntityManagerInterface $entityManager, string $className = null)
    {
        $this->entityManager = $entityManager;
        $this->className = $className ?? Config::class;
    }

    /**
     * Store a config by persisting it. This method will not call flush
     *
     * @param Config $config
     *
     * @return bool
     */
    public function addConfig(Config $config): bool
    {
        $this->entityManager->persist($config);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getConfig(string $key): ?Config
    {
        return $this->entityManager->getRepository($this->className)->find($key);
    }

    /**
     * @inheritDoc
     */
    public function getConfigs(): array
    {
        return $this->entityManager->getRepository($this->className)->findAll();
    }

    /**
     * @inheritDoc
     */
    public function addConfigs(array $configs): void
    {
        foreach ($configs as $config) {
            $this->addConfig($config);
        }
    }

    /**
     * @inheritDoc
     */
    public function getRequiredConfig(string $key): Config
    {
        $config = $this->getConfig($key);

        if ($config !== null) {
            return $config;
        }

        throw new NotFoundException("Config $key was not found");
    }
}
