<?php

declare(strict_types=1);

namespace MittNett\Config;

/**
 * Represents a entity that can be configured by the config library.
 *
 * This entity will hold several config objects.
 *
 * @author Edvin Hultberg <hultberg@mittnett.net>
 */
interface Configurable extends StorageInterface
{
}
