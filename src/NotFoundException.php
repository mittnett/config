<?php

declare(strict_types=1);

namespace MittNett\Config;

use Exception;

/**
 * Thrown when a config is not found.
 *
 * @author Edvin Hultberg <hultberg@mittnett.net>
 */
class NotFoundException extends Exception
{
}
